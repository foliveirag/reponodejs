//console.log("holaMundo");
var express=require('express');
var app= express();
var bodyParser=require('body-parser');
var baseMlabUrl= "https://api.mlab.com/api/1/databases/fog-techu/collections/";
var mLabAPIKEY="apiKey=a5Da0kyllo2j8sJGnCIW8DLJY8zGrYi8";
var requestJson=require('request-json');

//user?f={"first_name":1,"last_name":1,"_id":0}&s={"last_name":1}&l=5&apiKey=a5Da0kyllo2j8sJGnCIW8DLJY8zGrYi8'
app.use(bodyParser.json());
var port=process.env.PORT || 3000;
app.listen(port);
console.log("API Molona escuchando en el puerto "+ port);

app.get("/apitechu/v1", function(req,res){
  console.log("GET /apitechu/v1");
  res.send({"msg":"hola appitechu"});
}

)
app.get("/apitechu/v1/users",
  function(req,res){
    console.log("GET /apitechu/v1/users");
    //res.sendFile('./Users.json')  # DEPRECATED
    res.sendFile('Users-pwd.json', {root:__dirname})
    //var users=require('./Users.json');
    //res.send(users);

}
)
app.post("/apitechu/v1/users",
  function(req,res){
    console.log("POST /apitechu/v1/users");
    //console.log(req.headers);
    console.log("firstName "+req.body.first_name);
    console.log("LastName "+req.body.last_name);
    console.log("Country "+req.body.country);
    var newUser={
      "firs_name":req.body.first_name,
      "last_name":req.body.last_name,
      "country":req.body.country
    }
    var users=require('./Users.json');
    users.push(newUser);
    writeUserDataToFile(users);
    var msg="usuario guardado correctamente";
    res.send({"msg": msg});
  //res.send(users);


}
)

app.delete("/apitechu/v1/users/:id",
  function(req,res){
    console.log("DELETE /apitechu/v1/users/:id");
    console.log(req.params.id);
    var users=require('./Users.json');
    users.splice(req.params.id -1,1);
    writeUserDataToFile(users);
    res.send("Borrrado" + req.params.id);
    //res.sendFile('./Users.json')  # DEPRECATED
    //res.sendFile('Users.json', {root:__dirname})
    //var users=require('./Users.json');
    //res.send(users);

}
)

function writeUserDataToFile(Data){
  var fs =require('fs');
  var JsonUserData=JSON.stringify(Data);
  fs.writeFile("./Users-pwd.json",JsonUserData,"utf8",
    function (err){
      if (err){
        var msg="error al escribir el fichero";
        console.log (msg);
      }else {
        var msg="usuario guardado correctamente";
        console.log(msg);
      }
        console.log ("Usuarios añadidos con exito");
    })

}

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req,res){
    console.log("parametros");
    console.log (req.params);
    console.log ("QUERY STRING");
    console.log(req.query);
    console.log("Body");
    console.log(req.body);
    console.log ("HEADERS");
    console.log(req.headers);

  }
)

/**
APIResT para login de Usuarios
Loga al usuario identificado por email y password
**/
app.post("/apitechu/v1/login",
  function(req,res){
    console.log("POST /apitechu/v1/login");
    var email=req.body.email;
    var password=req.body.password;
    var users=require('./Users-pwd.json');
    var msg="";
    for (user of users){
       if (user.email==email && user.password==password){
            user.logged=true;
            msg={"mensaje":"Login correcto",
                  "id":user.id};
            writeUserDataToFile(users);
            break;
       }else{
           msg={"mensaje":"Login incorrecto"};
       }
    }
    res.send(msg);
}
)
/**
APIResT para logout de Usuarios
Desloga al usuario con Id y logged true
**/

app.post("/apitechu/v1/logout",
  function(req,res){
    console.log("POST /apitechu/v1/logout");
    var id=req.body.id;
    var users=require('./Users-pwd.json');
    var msg="";
    for (user of users){
       if (user.id==id && user.logged==true){
            delete user.logged;
            msg={"mensaje":"Logout correcto",
                  "id":user.id};
            writeUserDataToFile(users);
            break;
       }else{
         msg={"mensaje":"Logout incorrecto"};
       }
    }
    res.send(msg);
}
)


app.get("/apitechu/v2/users",
  function(req,res){
    console.log("GET /apitechu/v2/users");
    var httpclient=requestJson.createClient(baseMlabUrl);
    console.log("Cliente HTTP creado");
    httpclient.get("user?"+mLabAPIKEY,
    function(err,resMlab,body){
        var response=!err ? body :{
          "msg":"error obteniendo usuarios"
        }
      res.send(response)


  }

);

}
)







app.get("/apitechu/v2/users/:id",
  function(req,res){
    console.log("GET /apitechu/v2/users/:id");
    var id=req.params.id;
    var query='q={"id":'+id+'}';
    var httpclient=requestJson.createClient(baseMlabUrl);
    console.log("Cliente HTTP creado");

    httpclient.get("user?"+query +"&"+mLabAPIKEY,

    function(err,resMlab,body){
      //**  var response=!err ? body :{
        //  "msg":"error obteniendo usuarios"
      //  }
      if (err) {
        response= {
          "msg":"Error obtenidendo usuario"
        }
        res.status(500);
      }else{
          if (body.length>0) {
            response= body;

          }else{
            response= {
              "msg":"Usuario no encontrado"
            }
            res.status(404);
          }

      }

res.send(response);


  }

);

}
)
/**
APIResT para ver la cuenta de Usuarios, recibe el id de usuario como
parámetro y devuelve las cuentas asociadas.
**/


app.get("/apitechu/v2/users/:id/accounts",
  function(req,res){
    console.log("GET /apitechu/v2/users/:id/accounts");
    var id=req.params.id;
    var query='q={"id":'+id+'}';
    var httpclient=requestJson.createClient(baseMlabUrl);
    console.log("Cliente HTTP creado");

    httpclient.get("accounts?"+query +"&"+mLabAPIKEY,

    function(err,resMlab,body){
      //**  var response=!err ? body :{
        //  "msg":"error obteniendo usuarios"
      //  }
      if (err) {
        response= {
          "msg":"Error obtenidendo cuentas"
        }
        res.status(500);
      }else{
          if (body.length>0) {
            response= body;

          }else{
            response= {
              "msg":"cuenta no encontrada"
            }
            res.status(404);
          }

      }

res.send(response);


  }

);

}
)






/**
APIResT para login de Usuarios
Loga al usuario identificado por email y password
Petición PUT qeu lanza un GET a MLAB para con los datos recibidos eliminar el valor
Logged True.
**/
app.post("/apitechu/v2/login",
  function(req,res){
    console.log("PUT /apitechu/v2/login");
    var email=req.body.email;
    var password=req.body.password;
    var putBody='{"$set":{"logged":"true"}}';
      //console.log("GET /apitechu/v2/users/:id");

      var query='q={"email":"' + email + '","password":"'+password +'"}';
      var httpclient=requestJson.createClient(baseMlabUrl);
      console.log("Cliente HTTP creado");
      httpclient.get("user?"+query +"&"+mLabAPIKEY,
        function(err,resMlab,body){
        //**  var response=!err ? body :{
          //  "msg":"error obteniendo usuarios"
        //  }
        if (err) {
            response= {
              "msg":"Error validando usuario"
            }
            res.status(500);
        }else{
            if (body.length>0) {
              httpclient.put("user?"+query+"&"+mLabAPIKEY,JSON.parse(putBody));
              response = {"mensaje" : "login correcto", "idUsuario" : body[0].id };
            }else{
              response= {
                "msg":"Login Incorrecto"
              }
              res.status(404);
            }

        }

      res.send(response);


    }

  );
}

    //res.send(msg);

)

/**
APIResT para logout de Usuarios
Desloga al usuario con Id y logged true
Petición PUT qeu lanza un GET a MLAB para con los datos recibidos eliminar el valor
Logged True.
**/

app.put("/apitechu/v2/logout",
  function(req,res){
    console.log("PUT /apitechu/v2/logout");
    var id=req.body.id;
    var putBody='{"$unset":{"logged":""}}';
    var query='q={"id":' + id + ',"logged":"true"}';
    var httpclient=requestJson.createClient(baseMlabUrl);
    console.log("Cliente HTTP creado");
    httpclient.get("user?"+query +"&"+mLabAPIKEY,
      function(err,resMlab,body){
      //**  var response=!err ? body :{
        //  "msg":"error obteniendo usuarios"
      //  }
      if (err) {
          response= {
            "msg":"Error obtenidendo usuario"
          }
          res.status(500);
      }else{
          if (body.length>0) {
            httpclient.put("user?"+query+"&"+mLabAPIKEY,JSON.parse(putBody));
            response = {"mensaje" : "Logout correcto", "idUsuario" : body[0].id };
          }else{
            response= {
              "msg":"Logout Incorrecto"
            }
            res.status(404);
          }

      }

    res.send(response);


  }
)


}
)
